import Exceptions.MaxMistakesException;
import Exceptions.RepeatedInputException;

import java.util.Scanner;

public class Game {
    static Scanner kbd = new Scanner(System.in);
    private static Hangman game;

    public static void main(String[] args) {
        try {
            System.out.println("\nLet's Play Hangman!");
            begin();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.exit(0);
    }

    public static void begin() {
        int choice;
        do {
            try {
                getMenu();
                choice = Integer.parseInt(kbd.nextLine());
                if(choice == 1) {
                    startGame();
                } else if(choice == 2) {
                    System.out.println("\nThank you, " +game.getPlayer().getUName()+ "!");
                    System.out.println("Exiting Hangman...");
                } else {
                    System.out.println("Input 1 or 2 only.");
                }
            } catch (NumberFormatException e) {
                System.out.println("Input number only.");
                choice = 0;
            }
        } while(choice != 2);
    }

    public static void getMenu() {
        System.out.println("\n---------------------");
        System.out.printf("%12s%n", "Menu");
        System.out.println("---------------------");
        System.out.println("1. Play");
        System.out.println("2. Exit");
        System.out.print("> ");
    }

    public static void startGame() {
        System.out.println("\n---------------------");
        System.out.printf("%12s%n", "Hangman Game (Structure of Programming Language)");
        System.out.println("---------------------");
        System.out.print("Enter username: ");
        String username = kbd.nextLine();

        game = new Hangman(username);
        boolean playAgain;
        System.out.print("Welcome " + username +"!");
        do {
            playAgain = playGame();
        } while (playAgain);
    }

    public static boolean playGame() {
        boolean next = true, playAgain = true;
        char letter;
        int mistakes = 0, numberOfZero = 0;
        int[] index = new int[0];
        StringBuilder mysteryWord = new StringBuilder();

        System.out.println("\nHere is the mystery word you will guess.\n");
        int wordLength = game.getMysteryWordLength();

        for(int i = 0; i < wordLength; i++){
            Character ch1 = game.getPlayer().getMysteryWord().getWord().charAt(i);
            if(ch1.equals(' ')){
                mysteryWord.append(' ');
            } else {
                mysteryWord.append("*");
            }
        }
        System.out.println(mysteryWord);

        do {
            letter = getInput();
            try {
                numberOfZero = 0;
                index = null;
                index = game.checkInputLetter(letter);
                next = true;
            } catch (MaxMistakesException e) {
                mistakes = game.getNumMistakes();
                displayHangmanImage(mistakes);
                System.out.println("\nGame over.");
                System.out.println("You did not guess the mystery word: " + game.getCorrectWord());
                break;
            } catch (RepeatedInputException e) {
                System.out.println("Do not repeat input, enter other letter.");
                next = false;
            }

            if(next) {
                for (int i = 0; i < index.length; i++) {
                    if (index[i] == 1) {
                        mysteryWord.setCharAt(i, letter);
                    } else {
                        numberOfZero++;
                    }
                }

                if (numberOfZero == mysteryWord.length()) {
                    System.out.println("Guess another letter.\n");
                } else {
                    System.out.println("That's correct!\n");
                }

                System.out.println(mysteryWord);
                if(game.checkIfCorrectGuess(mysteryWord.toString())){
                    System.out.println("\nCONGRATULATIONS! you already guessed the word " +
                            game.getCorrectWord().toUpperCase() + ".");
                    break;
                }
                mistakes = game.getNumMistakes();
                displayHangmanImage(mistakes);
            }
        } while (mistakes < 5);

        do {
            System.out.print("\nWould you like to play again? (y/n) ");
            letter = kbd.nextLine().charAt(0);
            if(letter == 'y' || letter == 'Y') {
                game.playAgain();
                playAgain = true;
            } else if(letter == 'n' || letter == 'N') {
                playAgain = false;
            } else {
                System.out.println("Input y or n only.");
                next = false;
            }
        } while(!next);
        return playAgain;
    }

    private static char getInput() {
        boolean done = false;
        char input = 'x';
        do {
            try {
                System.out.println("Hint: "+ game.getPlayer().getMysteryWord().getDefinition());
                System.out.print("\nEnter a single character: ");
                input = kbd.nextLine().charAt(0);
                if (Character.isDigit(input)) {
                    System.out.print("Letters only.\n");
                } else if (Character.isLetter(input)) {
                    done = true;
                } else {
                    System.out.print("Letters only.\n");
                }
            } catch (StringIndexOutOfBoundsException stringIndexOutOfBoundsException) {
                System.out.println("Input correctly.");
            }
        } while(!done);
        return input;
    }

    private static void displayHangmanImage(int num) {
        switch (num) {
            case 0 -> {
                System.out.println("   ____________");
                System.out.println("   |");
                System.out.println("   |");
                System.out.println("   |");
                System.out.println("   |");
                System.out.println("   |");
                System.out.println("   |");
                System.out.println("   | ");
                System.out.println("___|___");
            }
            case 1 -> {
                System.out.println("   ____________");
                System.out.println("   |          _|_");
                System.out.println("   |         /   \\");
                System.out.println("   |        |     |");
                System.out.println("   |         \\_ _/");
                System.out.println("   |");
                System.out.println("   |");
                System.out.println("   |");
                System.out.println("___|___");
            }
            case 2 -> {
                System.out.println("   ____________");
                System.out.println("   |          _|_");
                System.out.println("   |         /   \\");
                System.out.println("   |        |     |");
                System.out.println("   |         \\_ _/");
                System.out.println("   |           |");
                System.out.println("   |           |");
                System.out.println("   |");
                System.out.println("___|___");
            }
            case 3 -> {
                System.out.println("   ____________");
                System.out.println("   |          _|_");
                System.out.println("   |         /   \\");
                System.out.println("   |        |     |");
                System.out.println("   |         \\_ _/");
                System.out.println("   |          _|_");
                System.out.println("   |         / | \\");
                System.out.println("   | ");
                System.out.println("___|___");
            }
            case 4 -> {
                System.out.println("   _____________");
                System.out.println("   |          _|_");
                System.out.println("   |         /   \\");
                System.out.println("   |        |     |");
                System.out.println("   |         \\_ _/");
                System.out.println("   |          _|_");
                System.out.println("   |         / | \\");
                System.out.println("   |          /");
                System.out.println("___|___      /");
            }
            case 5 -> {
                System.out.println("   ____________ ");
                System.out.println("   |          _|_");
                System.out.println("   |         /   \\");
                System.out.println("   |        |     |");
                System.out.println("   |         \\_ _/");
                System.out.println("   |          _|_");
                System.out.println("   |         / | \\");
                System.out.println("   |          / \\ ");
                System.out.println("___|___      /   \\");
            }
        }
    }
}
