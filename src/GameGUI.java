import Exceptions.MaxMistakesException;
import Exceptions.RepeatedInputException;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Objects;

public class GameGUI {
    private static Hangman game;
    private int numTries = 5;
    char[] guess;
    private final JFrame frame;
    private static JFrame launchFrame;
    private JFrame lostFrame;
    private JFrame wonFrame;
    private final JTextField usernameField;
    private static JTextField launchTextField;
    private final JButton playButton;
    private final JButton clearButton;

    public static void main(String[] args) {
        GameGUI gameGUI = new GameGUI();
    }
    public GameGUI() {
        JPanel panel = new JPanel();
        frame = new JFrame("Hangman Game");
        frame.getContentPane().setLayout(new BorderLayout());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.setSize(300, 250);
        frame.setLocationRelativeTo(null);
        frame.add(panel);

        panel.setLayout(null);
        JLabel usernameLabel = new JLabel("Username: ");
        usernameLabel.setBounds(10, 20, 80, 25);
        panel.add(usernameLabel);
        usernameField = new JTextField(20);
        usernameField.setBounds(100, 20, 165, 25);
        panel.add(usernameField);
        clearButton = new JButton("Clear fields");
        clearButton.setBounds(65, 120, 165, 25);
        clearButton.setBackground(Color.RED);
        panel.add(clearButton);
        playButton = new JButton("Play");
        playButton.setBounds(65, 155, 165, 25);
        playButton.setBackground(Color.green);
        panel.add(playButton);

        playButton.addActionListener(new PlayListener());
        clearButton.addActionListener(new ClearListener());
        frame.setVisible(true);
    }

    public void launchGameView(String guesses, int triesLeft) throws IOException {
        String fileName = "";
        launchFrame = new JFrame("Hangman Game");
        JPanel panel = new JPanel();
        launchFrame.getContentPane().setLayout(new BorderLayout());
        launchFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        launchFrame.setResizable(false);
        launchFrame.setSize(580, 550);
        launchFrame.setLocationRelativeTo(null);
        launchFrame.add(panel);
        panel.setLayout(null);

        switch (triesLeft){
            case 0:
            case 1:
                fileName = "hangman4";
                break;
            case 2:
                fileName = "hangman3";
                break;
            case 3:
                fileName = "hangman2";
                break;
            case 4:
                fileName = "hangMan1";
                break;
            case 5:
                fileName = "hangMan0";
                break;
        }

        BufferedImage img = ImageIO.read(new  File("res/img/" + fileName + ".png"));
        JLabel pic = new JLabel(new ImageIcon(img));
        pic.setBounds(170, -10, 209, 366);
        panel.add(pic);
        panel.revalidate();
        panel.repaint();

        JLabel menuLabel = new JLabel("Your word: " + guesses);
        menuLabel.setFont(new Font("Dialog", Font.BOLD, 20));
        menuLabel.setBounds(75, 40, 400 ,35);
        panel.add(menuLabel);

        String triesLabelText = "";
        if(triesLeft > 1)
            triesLabelText = "You have " + triesLeft + " tries left";
        if (triesLeft == 1)
            triesLabelText = "You have " + triesLeft + " try left!";

        JLabel triesLabel = new JLabel(triesLabelText);
        triesLabel.setFont(new Font("Dialog", Font.BOLD, 20));
        triesLabel.setBounds(180, 280, 400 ,25);
        panel.add(triesLabel);

        JLabel instruction = new JLabel("Please enter a letter below");
        instruction.setBounds(200, 300, 400, 25);
        panel.add(instruction);

        launchTextField = new JTextField();
        launchTextField.setBounds(250, 320, 50, 25);
        panel.add(launchTextField);

        JButton lookButton = new JButton("Check Letter");
        lookButton.setBounds(200, 350, 150, 25);
        lookButton.setMargin(new Insets(0,0,0,0));
        lookButton.addActionListener(new LookActionListener());
        panel.add(lookButton);

        JButton guessButton = new JButton("Guess the word");
        guessButton.setBounds(200, 380, 150, 25);
        guessButton.addActionListener(new GuessActionListener());
        panel.add(guessButton);

        JButton hintButton = new JButton("Hint");
        hintButton.setBounds(200, 410, 150, 25);
        hintButton.addActionListener(new HintListener());
        panel.add(hintButton);

        JButton giveUpButton = new JButton("Give up");
        giveUpButton.setBounds(200, 410, 150, 25);
        giveUpButton.addActionListener(new GiveUpListener());
        panel.add(giveUpButton);

        launchFrame.setVisible(true);

        launchTextField.getText();
    }

    public void lostGame(String word, ActionListener yes, ActionListener no) throws IOException {
        lostFrame = new JFrame("Hangman - Game Lost");
        JPanel panel = new JPanel();
        lostFrame.getContentPane().setLayout(new BorderLayout());
        lostFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        lostFrame.setResizable(false);
        lostFrame.setSize(600, 500);
        lostFrame.setLocationRelativeTo(null);
        lostFrame.add(panel);
        panel.setLayout(null);

        BufferedImage img = ImageIO.read(new File("res/img/hangman5.png"));
        JLabel pic = new JLabel(new ImageIcon(img));
        pic.setBounds(180, -10, 209, 366);
        panel.add(pic);
        panel.revalidate();
        panel.repaint();

        JLabel menuLabel = new JLabel("You Lost!");
        menuLabel.setFont(new Font("Dialog", Font.BOLD, 20));
        menuLabel.setBounds(240, 40, 400 ,25);
        panel.add(menuLabel);

        JLabel triesLabel = new JLabel("Your word was: " + word);
        triesLabel.setFont(new Font("Dialog", Font.BOLD, 20));
        triesLabel.setBounds(150, 280, 400 ,25);
        panel.add(triesLabel);

        JLabel tryAgainLabel = new JLabel("Would you like to try again?");
        tryAgainLabel.setFont(new Font("Dialog", Font.BOLD, 20));
        tryAgainLabel.setBounds(150, 320, 400 ,25);
        panel.add(tryAgainLabel);

        JButton okButton = new JButton("Yes");
        okButton.setBounds(150, 380, 80, 25);
        okButton.setMargin(new Insets(0,0,0,0));
        okButton.addActionListener(yes);
        panel.add(okButton);

        JButton noButton = new JButton("No");
        noButton.setBounds(320, 380, 80, 25);
        noButton.setMargin(new Insets(0,0,0,0));
        noButton.addActionListener(no);
        panel.add(noButton);

        lostFrame.setVisible(true);
    }

    public void wonGame(String word, ActionListener yes, ActionListener no) throws IOException {
        wonFrame = new JFrame("Hangman - Game Won");
        JPanel panel = new JPanel();
        wonFrame.getContentPane().setLayout(new BorderLayout());
        wonFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        wonFrame.setResizable(false);
        wonFrame.setSize(600, 500);
        wonFrame.setLocationRelativeTo(null);
        wonFrame.add(panel);
        panel.setLayout(null);

        BufferedImage img = ImageIO.read(new File("res/img/hangman32.png"));
        JLabel pic = new JLabel(new ImageIcon(img));
        pic.setBounds(180, -10, 209, 366);
        panel.add(pic);
        panel.revalidate();
        panel.repaint();

        JLabel menuLabel = new JLabel("Congratulations!");
        menuLabel.setFont(new Font("Dialog", Font.BOLD, 20));
        menuLabel.setBounds(205, 40, 400 ,25);
        panel.add(menuLabel);

        JLabel triesLabel = new JLabel("You guessed the word: " + word);
        triesLabel.setFont(new Font("Dialog", Font.BOLD, 20));
        triesLabel.setBounds(130, 280, 400 ,25);
        panel.add(triesLabel);

        JLabel tryAgainLabel = new JLabel("Would you like to play again?");
        tryAgainLabel.setFont(new Font("Dialog", Font.BOLD, 20));
        tryAgainLabel.setBounds(140, 320, 400 ,25);
        panel.add(tryAgainLabel);

        JButton okButton = new JButton("Yes");
        okButton.setBounds(150, 380, 80, 25);
        okButton.setMargin(new Insets(0,0,0,0));
        okButton.addActionListener(yes);
        panel.add(okButton);

        JButton noButton = new JButton("No");
        noButton.setBounds(320, 380, 80, 25);
        noButton.setMargin(new Insets(0,0,0,0));
        noButton.addActionListener(no);
        panel.add(noButton);

        wonFrame.setVisible(true);
    }

    public char[] hideWords(int wordLength){
        char[] hiddenWord = new char[wordLength];
        for (int i = 0; i < wordLength; i++){
            Character ch1 = game.getPlayer().getMysteryWord().getWord().charAt(i);
            if(ch1.equals(' ')){
                hiddenWord[i] = ' ';
            } else {
                hiddenWord[i] = '*';
            }
        }
        return hiddenWord;
    }

    public String makeString(char[] array){
        StringBuilder string = new StringBuilder();
        for (char c : array) { // Assign empty dashes at start "_ _ _ _ _ _ _ _"
            string.append(c).append(" ");
        }
        return string.toString();

    }

    public boolean check(char c, char[] randomWord){
        char c1 = Character.toUpperCase(c);
        boolean foundLetter = false;
        for (char value : randomWord) {
            if (value == c1) {
                foundLetter = true;
                break;
            }
        }
        return foundLetter;
    }

    public char[] switchLetter(char[] mysteryWord, int[] index, char c){
        for (int i = 0; i < index.length; i++) {
            if (index[i] == 1) {
                mysteryWord[i] = c;
            }
        }
        return mysteryWord;
    }

    public boolean checkIfComplete(char[] array){
        boolean condition = true;
        for(int i=0; i<array.length; i++){
            if(array[i] == '*'){
                condition = false;
            }
        }
        return condition;
    }

    /*
    ActionListener classes
     */
    class PlayListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                String username = usernameField.getText();
                frame.dispose();
                JOptionPane.showMessageDialog(null, "Hello, " + username + "!\nLet's play Hangman!");
                game = new Hangman(username);

                guess = hideWords(game.getMysteryWordLength());
                launchGameView(makeString(guess),numTries);
            } catch (IOException ioException) {
                System.out.println("Something is wrong.");
                ioException.printStackTrace();
            }
        }
    }

    class ClearListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            usernameField.setText("");
        }
    }

    class GiveUpListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            launchFrame.dispose();
            try {
                lostGame(game.getCorrectWord(), new YesLostActionListener(), new NoLostActionListener());
            } catch (IOException ex) {
                ex.printStackTrace();
            }

        }
    }

    static class HintListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            JOptionPane.showMessageDialog(null, "Hint: " + game.getPlayer().getMysteryWord().getDefinition());
        }
    }

    class GuessActionListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            String wordGuess = (String) JOptionPane.showInputDialog(null, "Enter your word guess");

            if (wordGuess == null || wordGuess == "") {
                JOptionPane.showMessageDialog(null, "Please don't leave the field blank.");
                try {
                    launchFrame.dispose();
                    //guess = hideWords(game.getPlayer().getMysteryWord().getWord().length());
                    launchGameView(makeString(guess),numTries);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            } else if (game.checkIfCorrectGuess(wordGuess)){
                launchFrame.dispose();
                try {
                    wonGame(game.getCorrectWord(), new YesWonActionListener(), new NoWonActionListener());
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            } else {
                JOptionPane.showMessageDialog(null, "Your guess is incorrect");
                numTries--;
                launchFrame.dispose();
                try {
                    if (numTries < 1){
                        lostGame(game.getCorrectWord(), new YesLostActionListener(), new NoLostActionListener());
                    } else {
                        launchGameView(makeString(guess),numTries);
                    }
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    class YesWonActionListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            wonFrame.dispose();
            game.playAgain();
            numTries = 5;
            try {
                guess = hideWords(game.getMysteryWordLength());
                launchGameView(makeString(guess),numTries);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    class YesLostActionListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            lostFrame.dispose();
            game.playAgain();
            numTries = 5;
            try {
                guess = hideWords(game.getMysteryWordLength());
                launchGameView(makeString(guess),numTries);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    class NoWonActionListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            wonFrame.dispose();
            JOptionPane.showMessageDialog(null, "Thank you for playing.");
            System.exit(0);
        }
    }

    class NoLostActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            lostFrame.dispose();
            JOptionPane.showMessageDialog(null, "Thank you for playing.");
            System.exit(0);
        }
    }

    class LookActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            boolean isComplete;
            launchFrame.dispose();
            try {

                boolean confirmed = Character.isLetter(launchTextField.getText().charAt(0));
                if (!confirmed) {
                    JOptionPane.showMessageDialog(null, "Please use only letters.");
                    launchGameView(makeString(guess),numTries);
                }

                if (confirmed) {
                    if (game.getNumMistakes() < 5) {
                        boolean foundLetter = check(launchTextField.getText().charAt(0), game.getCorrectWord().toCharArray()); //check if the input is in the word
                        int[] index = game.checkInputLetter(launchTextField.getText().charAt(0));  //checks the input letter
                        guess = switchLetter(guess, index, launchTextField.getText().charAt(0));

                        if (!foundLetter) {
                            numTries--;
                        }

                        isComplete = checkIfComplete(guess);
                        if (isComplete) {
                            wonGame(game.getCorrectWord(), new YesWonActionListener(), new NoWonActionListener());
                        } else {
                            launchGameView(makeString(guess),numTries);
                        }
                    } else {
                        lostGame(game.getCorrectWord(), new YesLostActionListener(), new NoLostActionListener());
                    } //end else
                } //end if confirmed and not repeated
            } catch (IOException ioe){
                ioe.printStackTrace();
            } catch (RepeatedInputException rie){
                JOptionPane.showMessageDialog(null, "You have already tried that input!");
                try {
                    //guess = hideWords(game.getPlayer().getMysteryWord().getWord().length());
                    launchGameView(makeString(guess),numTries);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            } catch (MaxMistakesException max) {
                try {
                    lostGame(game.getCorrectWord(), new YesLostActionListener(), new NoLostActionListener());
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            } catch (StringIndexOutOfBoundsException stringIndexOutOfBoundsException){
                JOptionPane.showMessageDialog(null, "Please don't leave the field blank.");
                try {
                    launchGameView(makeString(guess),numTries);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }
}
