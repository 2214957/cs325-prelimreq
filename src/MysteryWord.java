public class MysteryWord {
    private String word;
    private String definition;

    public MysteryWord(String word, String definition) {
        this.word = word;
        this.definition = definition;
    }

    public String getWord() {
        return word;
    }

    public String getDefinition() {
        return definition;
    }
}
