import java.util.ArrayList;
import java.util.List;

public class Player {
    private String username;
    private MysteryWord word;
    private int mistakes;
    private List<Character> inputLetters;
    private List<String> guessedWords;

    /**
     * Constructor method
     */
    public Player(String uName) {
        username = uName;
        this.mistakes = 0;
        inputLetters = new ArrayList<>();
        guessedWords = new ArrayList<>();
    }

    /**
     * Getter methods
     */
    public String getUName() { return username; }
    public MysteryWord getMysteryWord() { return word; }
    public int getMistakes() { return mistakes; }
    public List<Character> getInputLetters() { return inputLetters; }
    public List<String> getGuessedWords() { return guessedWords; }

    /**
     * Setter methods
     */
    public void setMysteryWord(MysteryWord word) {
        this.word = word;
    }
    public void setMistakes(int mistakes) {
        this.mistakes = mistakes;
    }
}
