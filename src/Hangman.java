import Exceptions.MaxMistakesException;
import Exceptions.RepeatedInputException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Hangman {
    private Player player;
    private List<MysteryWord> words;

    public Player getPlayer() {
        return player;
    }

    public Hangman(String uName) {
        player = new Player(uName);
    }
    public int getMysteryWordLength() {
        Random random = new Random();
        MysteryWord randomMysteryWord;
        int rWordLength;
        boolean done;

        getWords();
        do {
            int randomNumber = random.nextInt(words.size());
            randomMysteryWord = words.get(randomNumber);
            assert player != null;
            if (!(player.getGuessedWords().contains(randomMysteryWord.getWord()))) {
                player.setMysteryWord(randomMysteryWord);
                done = true;
            } else {
                done = false;
            }
        } while(!done);
        rWordLength = randomMysteryWord.getWord().length();
        return rWordLength;
    }

    public int getNumMistakes(){
        return player.getMistakes();
    }

    public int[] checkInputLetter(char letter) throws MaxMistakesException, RepeatedInputException {
        String word = getCorrectWord();
        int[] index = new int[word.length()];
        int numberOfZero = 0;
        char w;

        // if the letter is already in list of guessed letters, call errorMessage method
        if (player.getInputLetters().contains(letter)) {
            throw new RepeatedInputException();
        } else { // if letter is not still in guessed letter list
            // locate the letter in word and marked the index as 1 if found
            player.getInputLetters().add(letter);
            for (int i = 0; i < word.length(); i++) {
                w = word.charAt(i);
                if (w == Character.toUpperCase(letter)) {
                    index[i] = 1;
                }
            }

            // count the number of zero in the index[] value
            for (int x = 0; x < word.length(); x++) {
                if (index[x] == 0) {
                    numberOfZero++;
                }
            }

            // if the number of zero in index is equal to the length of the given word
            // then it means the input letter does not match any letter from the word
            if (numberOfZero == word.length()) {
                // add 1 to the mistake/s of player
                player.setMistakes(player.getMistakes() + 1);
                // if player reached the max number of mistakes, throw exception
                if (player.getMistakes() == 5) {
                    throw new MaxMistakesException();
                }
            }
        }
        return index;
    }

    public String getCorrectWord() {
        return player.getMysteryWord().getWord();
    }

    public boolean checkIfCorrectGuess(String guessWord) {
        boolean isCorrect = false;
        String word;

        word = player.getMysteryWord().getWord();

        if (word.equals(guessWord.toUpperCase())) {
            isCorrect = true;
        }

        return isCorrect;
    }

    public void playAgain() {
        player.setMistakes(0);
        player.getInputLetters().clear();
    }

    //Reads the words.txt file and returns list of all words
    private void getWords() {
        File file = new File("res/terminologies.csv");
        BufferedReader fileReader;
        words = new ArrayList<>();

        try {
            fileReader = new BufferedReader(new FileReader(file));

            String line;
            while ((line = fileReader.readLine()) != null) {
                String[] values = line.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
                MysteryWord word = new MysteryWord(values[0], values[1]);
                words.add(word);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
